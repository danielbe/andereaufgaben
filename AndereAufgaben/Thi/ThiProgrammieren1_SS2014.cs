﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndereAufgaben
{
    internal class ThiProgrammieren1_SS2014
    {
        internal string A2_Strings_Spleissen(string unspliced, string begin, string end)
        {
            StringBuilder sb = new StringBuilder();

            while (unspliced.Length > 0)
            {
                int? from = A2_Strings_FirstOccurrence(unspliced, begin);
                if (null == from)
                {
                    sb.Append(unspliced);
                    break;
                }

                int? to = A2_Strings_FirstOccurrence(unspliced.Substring(from ?? -1), end);

                if (sb.Length < 1 && null != from)
                    sb.Append(unspliced.Substring(0, from ?? -1));

                if (null != from && null != to)
                {
                    //sb.Append(unspliced.Substring(from ?? -1, to ?? -1));
                    unspliced = unspliced.Substring(to ?? -1);
                }
            }

            return sb.ToString();
        }

        //nicht verlangt, aber hier, damit das Programm läuft
        internal int? A2_Strings_FirstOccurrence(string s, string find)
        {
            int i = s.IndexOf(find);

            if (i != -1)
                return i;
            else
                return null;
        }
    }
}
