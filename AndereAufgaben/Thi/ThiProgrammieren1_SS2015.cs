﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndereAufgaben
{
    class ThiProgrammieren1_SS2015
    {
        public void A2_Algorithmus()
        {
            //Programm, das zyklisch vorzeichenlose ganze Zahlen einliest
            //und ziffernweise als Textform ausgibt
            //Eingabe "0" führt zum Beenden des Programms
            //Bsp: Eingabe "298" führt zu "zwei neun acht" 
            StringBuilder sb = new StringBuilder();

            string[] numbers = new string[]
            {
                "null", "eins", "zwei", "drei", "vier", "fünf",
                "sechs", "sieben", "acht", "neun"
            };

            while (true)
            {
                Console.WriteLine("Geben Sie Ihre Zahl ein");
                string input = Console.ReadLine();
                if (input.Equals("0")) break;
                foreach (char c in input)
                {
                    if (int.TryParse(c.ToString(), out int nr))
                    {
                        sb.Append(numbers[nr] + " ");
                    }
                }
                Console.WriteLine(sb.ToString().Trim());
                sb.Clear();
            }
        }

        public string A3_Arrays(string[] array, char inBetween)
        {
            if (array.Length == 0) return null;
            StringBuilder sb = new StringBuilder();
            foreach(string s in array)
            {
                sb.Append(s + inBetween);
            }
            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }

        public struct A4_List_Element
        {
            public string Data;
            public struct Next { };
        }

        public void A4_List_PrintMethod(A4_List_Element element)
        {
        }

        public void A4_List_AsList()
        {

        }
    }
}
