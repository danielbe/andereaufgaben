﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndereAufgaben
{
    class EveryCharTwice
	{
		public static string Normal(string input)
		{
			StringBuilder sb = new StringBuilder();
			foreach (char c in input)
			{
				sb.Append(string.Format("{0}{0}", c));
			}

			return sb.ToString();
		}

		public static string OneLineButSlow(string input)
			=> string.Join("", input.Select(c => $"{c}{c}"));
	}
}
