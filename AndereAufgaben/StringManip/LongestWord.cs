﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndereAufgaben
{
    class LongestWord
	{
		public static string WithSplit(string input) //faster
		{
			string[] ar = input.Split(' ');
			StringBuilder longest = new StringBuilder();

			foreach (string s in ar)
			{
				if (s.Length > longest.Length)
				{
					longest.Clear();
					longest.Append(s);
				}
			}
			return longest.ToString();
		}

		public static string NoSplit(string input) //slower
		{
			StringBuilder sb = new StringBuilder();
			StringBuilder longest = new StringBuilder();

			foreach (char c in input)
			{
				if (c != ' ')
				{
					sb.Append(c);
				}
				else
				{
					if (sb.Length > longest.Length)
						longest = sb;
					sb.Clear();
				}
			}

			return longest.ToString();
		}
	}
}
