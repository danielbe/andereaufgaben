﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndereAufgaben
{
    class Boomerang
    {
        //return array with all boomerangs
        //boomerang consists of 3 numbers
        //first number = third number
        //second number is different

        public static int[][] Get(int[] input)
        {
            if (input.Length < 3)
                return new int[0][];

            List<int[]> list = new List<int[]>();

            int stopAt = input.Length - 2;
            for (int i = 0; i < stopAt; i++)
            {
                if (input[i] == input[i + 2] && input[i] != input[i + 1])
                {
                    list.Add(new int[] { input[i], input[i + 1], input[i] });
                }
            }

            return list.ToArray();
        }

        public static int GetCount(int[] input)
        {
            if (input.Length < 3)
                return 0;

            int count = 0;

            int stopAt = input.Length - 2;
            for (int i = 0; i < stopAt; i++)
            {
                if (input[i] == input[i + 2] && input[i] != input[i + 1])
                {
                    count++;
                }
            }

            return count;
        }
    }
}
