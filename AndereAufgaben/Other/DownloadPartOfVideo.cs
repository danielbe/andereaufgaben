﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AndereAufgaben
{
    class DownloadPartOfVideo
    {
        internal static void Initialize(string[] args)
        {
            if (null != args && args.Length >= 2)
            {
                string thirdArg = args.Length == 3 ? FormatTime(args[2]) : null;
                Start(args[0], FormatTime(args[1]), thirdArg);
            }
            else
            {
                Console.WriteLine("Usage:");
                Console.WriteLine("URL startTime downloadLength:");
                Console.WriteLine("https://www.youtube.com/watch?v=123456 4m 1h24m6s");
                Console.WriteLine("downloads the first 5 minutes");
                Console.WriteLine("youtube-dl and ffmpeg have to be in same directory");
            }
            //Console.ReadLine();
        }

        private static void Start(string url, string startAt, string stopAfter)
        {
            //get urls via youtube-dl
            Process process = new Process
            { //.\youtube-dl.exe https://www.youtube.com/watch?v=123456 -f bestvideo+bestaudio -g
                StartInfo =
                {
                  FileName = @".\youtube-dl.exe",
                  //FileName = @"D:\VideoDownloads\youtube-dl.exe",
                  Arguments = $"\"{url}\" -f bestvideo+bestaudio -g",
                  UseShellExecute = false,
                  RedirectStandardOutput = true,
                  CreateNoWindow = true
                }
            };
            process.Start();
            process.WaitForExit();
            //Console.WriteLine(process.StartInfo.FileName);
            string[] urls = new string[2];
            int i = 0;
            while (!process.StandardOutput.EndOfStream)
            {
                urls[i] = process.StandardOutput.ReadLine();
                i++;
            }

            //Console.WriteLine(urls[0]);
            //Console.WriteLine(urls[0]);

            //download part of video via ffmpeg
            string stopAfterArg = null != stopAfter ? $" -t { stopAfter}" : "";
            Process processFfmpeg = new Process
            {
                StartInfo =
                {
                  FileName = @".\ffmpeg.exe",
                  //FileName = @"D:\VideoDownloads\ffmpeg.exe",
                  Arguments = $"-ss {startAt} -i \"{urls[0]}\"{stopAfterArg} -ss {startAt} -i \"{urls[1]}\"{stopAfterArg} -shortest -y -c:v copy -c:a copy download.mp4",
                  UseShellExecute = false,
                  RedirectStandardOutput = true,
                  CreateNoWindow = true
                }
            };
            Console.WriteLine("Ffmpeg command:");
            Console.WriteLine(processFfmpeg.StartInfo.FileName + " " + processFfmpeg.StartInfo.Arguments);
            //Console.ReadLine();
            processFfmpeg.Start();
            processFfmpeg.WaitForExit();


            Console.WriteLine("done");
            Console.ReadLine();
        }

        private static string FormatTime(string time)
        {
            int hours = Duration(time, 'h');
            int minutes = Duration(time, 'm');
            int seconds = Duration(time, 's');

            return $"{hours}:{minutes}:{seconds}";
        }

        private static int Duration(string time, char divider)
        {
            int ret = 0;
            Regex regex = new Regex("[0-9]+[" + divider + "]{1}");
            Match match = regex.Match(time);
            if (match.Success)
            {
                string value = match.Value.Substring(0, match.Value.Length - 1);
                if (int.TryParse(value, out int result))
                    ret = result;
            }

            return ret;
        }
    }
}
