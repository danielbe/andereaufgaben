﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AndereAufgaben
{
    class HiddenTricks
    {
        internal static void Test()
        {

        }

        internal static void Short()
        {
            //cast, if cast fails -> null
            object obj = new object();
            HiddenTricks ht = obj as HiddenTricks;
            //instead of
            HiddenTricks ht2 = (HiddenTricks)obj;

            //use -1 if value1 is null
            int? value1 = 0;
            int result = value1 ?? default;


            //use keywords as variable names
            string @class = "";

            //$ @
        }
    }
}
