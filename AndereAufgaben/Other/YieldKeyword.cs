﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndereAufgaben
{
    class YieldKeyword
    {
        internal void Consumer()
        {
            //slower and more expensive (extra list)
            foreach (int i in Integers())
            {
                Console.WriteLine(i.ToString());
                if (i.Equals(8)) break; //will never get value 16
            }
            foreach (int i in Integers2())
            {
                Console.WriteLine(i.ToString());
                if (i.Equals(8)) break; //already got value 16
            }

            //slower
            Integers2().Take(3);
            //faster, because we do not get every value
            Integers().Take(3);
        }

        //gets called for every return
        //returns first yield return, remembers where left off
        //second call will return second yield return
        public IEnumerable<int> Integers()
        {
            yield return 1;
            yield return 2;
            yield return 4;
            yield return 8;
            yield return 16;
        }

        //gets called once, returns full thing
        public IEnumerable<int> Integers2()
        {
            return new List<int>
            {
                1, 2, 4, 8, 16
            };
        }
    }
}
