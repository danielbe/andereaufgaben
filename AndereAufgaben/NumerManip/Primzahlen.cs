﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndereAufgaben
{
    class Primzahlen
    {
        //Primzahlen zu einer vorgegebenen Obergrenze berechnen
        public List<int> Solution(int until)
        {
            Func<int, int, bool> NotDiv = (i, j) => i % j != 0;

            List<int> ret = new List<int>();

            for (int i = 0; i <= until; i++)
            {
                if (i < 4 || i == 5 || i == 7)
                    ret.Add(i);
                else
                {
                    if (NotDiv(i, 2) && NotDiv(i, 3) && NotDiv(i, 5) && NotDiv(i, 7))
                        ret.Add(i);
                }
            }
            return ret;
        }
    }
}
