﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndereAufgaben
{
    class Zahlensystemumrechner
    {
        public int BinToDec()
        {
            Console.WriteLine("Geben Sie Ihre Binärzahl ein");
            int.TryParse(Console.ReadLine(), out int bin);
            int dec = 0;
            int i = 0;

            while (bin > 0)
            {
                if (bin % 2 != 0)
                {
                    dec += (int)Math.Pow(2, i);
                }
                bin /= 10;
                i++;
            }

            return dec;
        }

        public string DecToBin()
        {
            Console.WriteLine("Geben Sie Ihre Dezimalzahl ein");
            int.TryParse(Console.ReadLine(), out int dec);

            Stack<int> bin = new Stack<int>();

            while (dec > 0)
            {
                bin.Push(dec % 2);
                dec /= 2;
            }

            StringBuilder ret = new StringBuilder();
            while (bin.Count > 0)
                ret.Append(bin.Pop());

            return ret.ToString();
        }

    }
}
