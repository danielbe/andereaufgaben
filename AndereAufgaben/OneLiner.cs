﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndereAufgaben
{
    class OneLiner
    {
		//not really fast in general, but in one line

		public static int Syllables(string input) 
			=> input.Count(x => x == '-') + 1;
		public static string EveryCharTwice(string input)
			=> string.Join("", input.Select(c => $"{c}{c}"));

    }
}
