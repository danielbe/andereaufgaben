﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndereAufgaben
{
    class SchereSteinPapierAlt
    {
        public void Method()
        {
            Console.WriteLine("Schere, Kieselstein, Papier oder Qabbrechen");
            char input = 'a';

            while (!input.Equals("Q"))
            {
                input = Console.ReadLine()[0];

                if (input.Equals('Q')) return;
                else if (input.Equals('S') || input.Equals('K') || input.Equals('P'))
                {
                    int result = new Random().Next(3);
                    if (result.Equals(0)) //player won
                    {
                        Console.Write("Sie haben gewonnen! Der Computer nahm ");
                        if (input.Equals('S')) Console.WriteLine("Papier");
                        else if (input.Equals('P')) Console.WriteLine("Stein");
                        else if (input.Equals('K')) Console.WriteLine("Schere");
                    }
                    else if (result.Equals(1)) //computer won
                    {
                        Console.Write("Sie haben verloren! Der Computer nahm ");
                        if (input.Equals('S')) Console.WriteLine("Stein");
                        else if (input.Equals('P')) Console.WriteLine("Schere");
                        else if (input.Equals('K')) Console.WriteLine("Papier");
                    }
                    else //draw
                    {
                        Console.WriteLine($"Unentschieden! Spieler und Computer nahmen {input}");
                    }
                }
                else Console.WriteLine("Bitte geben Sie S, K, P, oder Q ein");
            }
        }
    }
}
