﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndereAufgaben
{
    class SchereSteinPapier
    {
        int userPick;
        int opponentPick;

        public void Method()
        {
            Console.WriteLine("Schere, Kieselstein, Papier oder Qabbrechen");
            char input;
            do
            {
                input = Console.ReadLine()[0];
                OpponentsPick();
                CheckWinner();
            }
            while (NextPick(input));
        }

        private void CheckWinner()
        {
            string result = "";
            if (userPick.Equals(opponentPick)) result = "Unentschieden";
            else if (userPick.Equals((int)Option.Schere))
            {
                if (opponentPick.Equals((int)Option.Stein))
                    result = "Computer gewinnt";
                else if (opponentPick.Equals((int)Option.Papier))
                    result = "Spieler gewinnt";
            }
            else if (userPick.Equals((int)Option.Stein))
            {
                if (opponentPick.Equals((int)Option.Papier))
                    result = "Computer gewinnt";
                else if (opponentPick.Equals((int)Option.Schere))
                    result = "Spieler gewinnt";
            }
            else if (userPick.Equals((int)Option.Papier))
            {
                if (opponentPick.Equals((int)Option.Schere))
                    result = "Computer gewinnt";
                else if (opponentPick.Equals((int)Option.Stein))
                    result = "Spieler gewinnt";
            }
            Console.WriteLine(result);
        }

        private void OpponentsPick()
        {
            opponentPick = new Random().Next(3);
        }

        private bool NextPick(char pick)
        {
            bool ret = true;
            if (pick.Equals('S')) userPick = (int)Option.Schere;
            else if (pick.Equals('K')) userPick = (int)Option.Stein;
            else if (pick.Equals('P')) userPick = (int)Option.Papier;
            else if (pick.Equals('Q')) ret = false;
            return ret;
        }

        private enum Option
        {
            Schere, Stein, Papier
        }
    }
}
