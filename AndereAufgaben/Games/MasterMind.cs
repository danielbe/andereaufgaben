﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndereAufgaben
{
    class MasterMind
    {
        //Richtige Farben und korrekte Position muss erraten werden
        //Farben werden durch Zahlen dargestellt
        //Bsp:
        //Zufällige Kombination muss erraten werden (z.B. 12345)
        //Benutzer rät: 14578
        //1 ist an richtiger Stelle, 4 und 5 dabei
        //Benutzer wird 112 zurückgegeben (1 richtig, 2 genau)

        int[] iaSetValues;
        int[] iaColorValues;
        List<int> iaResult;
        int nrColors;
        int nrSpaces;
        bool allowColorMultipleTimes;

        public MasterMind(int nrColors, int nrSpaces, bool allowColorMultipleTimes)
        {
            this.nrColors = nrColors;
            this.nrSpaces = nrSpaces;
            this.allowColorMultipleTimes = allowColorMultipleTimes;
            iaSetValues = new int[this.nrSpaces];
            iaColorValues = new int[this.nrSpaces];
            for (int i = 0; i < nrSpaces; i++)
                iaColorValues[i] = -1;
            SetColorValues(this.nrColors, this.nrSpaces, this.allowColorMultipleTimes);
            iaResult = new List<int>();
        }

        public void Method()
        {
            int turnNr = 1;
            bool inputCorrect;

            do
            {
                do
                {
                    inputCorrect = false;
                    Console.WriteLine($"Geben Sie Ihren Rateversuch ein! ({nrSpaces} Zahlen)");
                    string userInput = Console.ReadLine();
                    int errorCode = InterpretInput(userInput);
                    if (errorCode.Equals((int)Codes.Ok))
                        inputCorrect = true;
                    //clear console wäre cool
                }
                while (!inputCorrect);
                CheckHits();
                Console.WriteLine(BuildOutputString(turnNr));
                turnNr++;
            }
            while (!GameWon());
        }

        private bool GameWon()
        {
            return iaResult.Count(i => i.Equals(2)).Equals(nrSpaces);
        }

        private string BuildOutputString(int moveNr)
        {
            StringBuilder s = new StringBuilder();
            s.Append($"{moveNr}. ");
            foreach (var c in iaSetValues)
                s.Append($"|{c}");

            s.Append("|   |");
            iaResult.ForEach(i => s.Append($"{i.ToString()}|"));
            return s.ToString();
        }
    
        private void CheckHits()
        {
            //     right color   right space
            //0    no            no
            //1    yes           no
            //2    yes           yes
            int hit = 0;
            iaResult.Clear();


            int length = iaColorValues.Length;
            int[] temp = new int[length];
            for (int i = 0; i < length; i++) temp[i] = iaSetValues[i];

            for (int i = 0; i < length; i++)
            {
                hit = 0;
                for (int j = 0; j < length; j++)
                {
                    if (iaColorValues[i].Equals(temp[i]))
                    {
                        hit = 2;
                        temp[i] = -1;
                        break;
                    }
                    else if (iaColorValues[i].Equals(temp[j]))
                    {
                        hit = 1;
                        temp[i] = -1;
                        break;
                    }
                }
                if (hit != 0) iaResult.Add(hit);
            }
            iaResult.Sort();
        }

        private void SetColorValues(int maxColors, int maxFields, bool doubleValues)
        {
            if (maxColors < maxFields && !doubleValues)
            {
                maxFields = maxColors;
                Console.WriteLine("Anzahl der Felder wurde angepasst. Benutzereingabe war inkorrekt");
            }

            Random random = new Random();
            if (maxColors > 10) maxColors = 10;

            int randomNr = 0;
            for (int i = 0; i < maxFields; i++)
            {
                do
                {
                    randomNr = random.Next(maxColors);
                }
                while (!doubleValues && NrAlreadyInArray(randomNr));
                iaColorValues[i] = randomNr;
            }
        }

        private bool NrAlreadyInArray(int nr) => iaColorValues.Any(e => e.Equals(nr));

        private int InterpretInput(string input)
        {
            int ret = (int)Codes.Ok;
            int inputLength = input.Length;
            if (input.Equals("Q")) ret = (int)Codes.GiveUp;
            else if (inputLength < nrSpaces) ret = (int)Codes.InputTooShort;
            else if (inputLength > nrSpaces) ret = (int)Codes.InputTooLong;
            else
            {
                for (int i = 0; i < inputLength; i++)
                {
                    if (int.TryParse(input[i].ToString(), out int inputI) && inputI < nrColors)
                        iaSetValues[i] = inputI;
                    else
                    {
                        ret = (int)Codes.InputWrong;
                        break;
                    }
                }
            }
            return ret;
        }

        private enum Codes
        {
            Ok, GiveUp, InputTooLong, InputTooShort, InputWrong 
        }
    }
}
