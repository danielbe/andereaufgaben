﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndereAufgaben
{
    class GuessTheNumber
    {
        int maxNumber;
        int nrToGuess;
        int amountOfTries;

        public GuessTheNumber(int maxNumber, int amountOfTries)
        {
            if (maxNumber > 1) this.maxNumber = maxNumber;
            else this.maxNumber = 10;

            this.amountOfTries = amountOfTries;
            nrToGuess = new Random().Next(this.maxNumber) + 1;
        }

        public void Method()
        {
            int currentTry = 0;
            int currentGuess;
            Console.WriteLine($"Eine Zahl zwischen 1 und {maxNumber}");
            while (currentTry < amountOfTries)
            {
                currentTry++;
                Console.Write($"{currentTry}. Versuch: Raten Sie die Zahl! ");
                while (!int.TryParse(Console.ReadLine(), out currentGuess)) ;
                if (currentGuess > nrToGuess) Console.WriteLine("Zu hoch!");
                else if (currentGuess < nrToGuess) Console.WriteLine("Zu tief!");
                else if (currentGuess == nrToGuess)
                {
                    Console.WriteLine("Glückwunsch! Sie haben die Zahl erraten");
                    break;
                }
            }
        }
    }
}
