﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndereAufgaben
{
    class GloBus
    {
        //1. Frage, welche Buskatgorie verwendet werden soll
        //2. Nach Eingabe werden Namen aller Passagiere für jeden Sitzplatz nacheinander eingetragen
        //   Leereingaben sind möglich
        //3. Nach Eingabe wird in Suchmodus gewechselt: Eingabe "P" oder "S"
        //   P: Nach Personen suchen, Name der Person auf eingegebener Sitznummer wird angezeigt
        //   S: Nach Sitz suchen, Genauer Treffer einer Person gibt Sitznummer zurück
        //   Fehlerhafte Eingaben mit "Falsche Sitzplatznummer", bzw. "Person nicht gefunden" kennzeichnen
        //4. Frage, ob erneut gesucht werden soll ("J")
        //   J: Zurück zu 3.
        //   Alles andere: Programm beenden
        //   Buskategorie - Hersteller / Typ - Sitzplätze
        //   1            - MB Sprinter      - 15
        //   2            - Iveco Team       - 20
        //   3            - MAN Clubline     - 30

        public void Solution()
        {
            //initialize buses
            Bus[] buses = new Bus[] {
            new Bus(1, "BM Sprinter", 15),
            new Bus(2, "Iveco Team", 20),
            new Bus(3, "MAN Clubline", 30)
            };

            Console.WriteLine("Welche Buskategorie soll verwendet werden?");
            int category = 0;
            while (category == 0 || category > buses.Length)
                int.TryParse(Console.ReadLine(), out category);
            category--;

            Console.WriteLine("Bitte den Namen der Passagiere eintragen");
            int seats = buses[category].Seats;
            for (int i = 1; i <= seats; i++)
            {
                Console.Write($"{i}: ");
                buses[category].PutPersonAtSeat(Console.ReadLine(), i);
            }

            Console.WriteLine("Suchmodus: \"P\" drücken, um nach einer Person zu suchen, \"S\" nach einem Platz");
            string search = "";
            while (!search.Equals("P") && !search.Equals("S"))
                search = Console.ReadLine();

            Console.Write("Sie befinden sich nun im ");
            bool retry = true;
            if (search.Equals("P"))
            {
                while (retry)
                {
                    Console.WriteLine("Personen-Suchmodus");
                    Console.WriteLine("Geben Sie den Sitzplatz ein und Sie erhalten den Namen");
                    int seat = 0;
                    while (seat <= 0 || seat > buses[category].Seats)
                        int.TryParse(Console.ReadLine(), out seat);
                    Console.WriteLine(buses[category].People[seat - 1]);
                    Console.WriteLine("Wollen Sie nochmal suchen? (J)");
                    retry = Console.ReadLine().Equals("J");
                }
            }
            else
            {
                while (retry)
                {
                    Console.WriteLine("Sitzplatz-Suchmodus");
                    Console.WriteLine("Geben Sie den Namen ein und Sie erhalten den Sitzplatz");
                    string person = Console.ReadLine();
                    foreach (string p in buses[category].People)
                    {
                        if (p.Equals(person))
                        {
                            Console.WriteLine(Array.IndexOf(buses[category].People, p) + 1);
                            break;
                        }
                    }
                    Console.WriteLine("Wollen Sie nochmal suchen? (J)");
                    retry = Console.ReadLine().Equals("J");
                }
            }
        }
    }

    class Bus
    {
        public int Category { get; set; }
        public string Type { get; set; }
        public int Seats { get; set; }
        public string[] People { get; set; }

        public Bus(int category, string type, int seats)
        {
            this.Category = category;
            this.Type = type;
            Seats = seats;
            People = new string[Seats];
        }

        public void PutPersonAtSeat(string person, int seat)
        {
            People[seat - 1] = person;
        }
    }
}
