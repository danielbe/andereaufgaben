﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AndereAufgaben
{
    class Gp4Performance
    {
        static List<string> files = new List<string>() { "Imola", "Nurburgring", "Suzuka", "Sakhir",
            "Monaco", "Barcelona", "Testing", "Silverstone", "Shanghai", "Sepang", "PreSeason",
            "Monza", "Montreal", "Melbourne", "MagnyCours", "Istanbul", "Interlagos", 
            "Indianapolis", "Hungaroring", "Hockenheim" };

        internal static void Write()
        {
            Console.WriteLine("Type name of source file");
            string source = Console.ReadLine();

            string contents = File.ReadAllText(source);

            foreach (string file in files)
            {
                File.WriteAllText($"2006-{file}.txt", contents);
            }
            File.WriteAllText("2006.txt", contents);

            Console.ReadKey();
        }
    }
}
